var express = require('express'),
    exphbs  = require('express3-handlebars'),
    jade    = require('jade'),

    routes = require('./lib/routes'),

    app = express(),
    hbs;


app.configure(function () {
    app.use(express.urlencoded());
    app.use(express.json());

    app.set('view engine', 'jade');
});

app.locals({
    title: "Angular VVT"
});

// -- ROUTES --
app.use(express.static('public/'));


app.get('/api/decode/:year?/:make?/:model?/:trim?', routes.api_decode);
app.get('/api/transactions', routes.api_transactions);


app.get('/', routes.home);

// START IT UP
app.listen(3000);
console.log('FalconVVT is listengin on: ' + 3000);
