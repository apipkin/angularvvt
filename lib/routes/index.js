var pageRoutes = require('./pages'),
    apiRoutes  = require('./api_v1');


Object.keys(pageRoutes).forEach(function (route) {
    module.exports[route] = pageRoutes[route];
});



// Add all api routes to exports
Object.keys(apiRoutes).forEach(function (route) {
    module.exports['api_' + route] = apiRoutes[route];
});