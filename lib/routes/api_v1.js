var decodeData = {
    '2000' : {
        'BMW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Chevrolet' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Lexus' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Mercedes' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'VW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        }
    },

    '2001' : {
        'BMW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Chevrolet' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Lexus' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Mercedes' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'VW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        }
    },

    '2002' : {
        'BMW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Chevrolet' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Lexus' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Mercedes' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'VW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        }
    },

    '2003' : {
        'BMW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Chevrolet' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Lexus' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Mercedes' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'VW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        }
    },

    '2004' : {
        'BMW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Chevrolet' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Lexus' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Mercedes' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'VW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        }
    },

    '2005' : {
        'BMW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Chevrolet' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Lexus' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'Mercedes' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        },
        'VW' : {
            'Alpaca'  : ['1 eye', '1 ear','Giant','Purple'],
            'Baboon'  : ['1 eye', '1 ear','Giant','Purple'],
            'Cheetah' : ['1 eye', '1 ear','Giant','Purple'],
            'Dolphin' : ['1 eye', '1 ear','Giant','Purple']
        }
    }
};


module.exports = {
    decode: function (req, res) {
        var year = req.params.year,
            make = req.params.make,
            model = req.params.model,
            trim = req.params.trim,
            data = decodeData,
            postfix = '.',
            type = 'years';

        if (year) {
            year = year.split('.')[0];
            data = decodeData[year];
            postfix += year.slice(-1);
            type = 'makes';
        }

        if (make) {
            make = make.split('.')[0];
            data = data[make];
            postfix += make.charAt(0);
            type = 'models';
        }

        if (model) {
            model = model.split('.')[0];
            data = data[model];
            postfix += model.charAt(0);
            type = 'trims';
        }

        if (typeof data.length === 'undefined') {
            data = Object.keys(data);
        } else {
            data = data.concat();
        }

        data.forEach(function (val, index, arr) {
            arr[index] = {
                id: val,
                name: val + postfix
            };
        });

        res.json({
            type: type,
            results: data
        });
    },
    transactions: function (req, res) {
        res.json({
            total: 100,
            results: [
                { date: 1396411200000, price: 38500, odometer: 34019,
                  condition: '2.0', engine: 'NON/N', color: 'Green',
                  type: 'Regular', region: 'Midwest',
                  auction: 'IN - Manheim Indianapolis'},
                { date: 1389934800000, price: 34234, odometer: 48777,
                  condition: '2.0', engine: '8CY/A', color: 'Yellow',
                  type: 'Regular', region: 'Southeast',
                  auction: 'TN - Manheim Nashville'},
                { date: 1391662800000, price: 41500, odometer: 65590,
                  condition: '1.0', engine: '8G/A',  color: 'White',
                  type: 'Regular', region: 'Northeast',
                  auction: 'PA - Manheim Pennsylvania'},
                { date: 1383800400000, price: 33200, odometer: 73230,
                  condition: '3.0', engine: '8G/A',  color: 'Green',
                  type: 'Lease',   region: 'Midwest',
                  auction: 'IL - Manheim Chicago'},
                { date: 1380772800000, price: 46000, odometer: 76917,
                  condition: '1.0', engine: '8G/A',  color: 'Red',
                  type: 'Regular', region: 'Midwest Coast',
                  auction: 'CA - Manheim Riverside'},
                { date: 1381982400000, price: 40000, odometer: 94474,
                  condition: '2.0', engine: '8G/A',  color: 'White',
                  type: 'Regular', region: 'Midwest Coast',
                  auction: 'CA - Manheim Riverside'}
            ]
        });
    }
};
