var vvtControllers = angular.module('vvtControllers', []);


vvtControllers.controller('DecoderCtrl', ['$scope', '$http', '$rootScope', function ($scope, $http, $rootScope) {
    // default values
    $scope.year;
    $scope.make;
    $scope.model;
    $scope.trim;
    $scope.vin;

    $http.get('/api/decode').success(apiCallback);


    $scope.ymmt = function () {
        var ymmt = 'Select base.';

        if ($scope.trim) {
            ymmt = [$scope.year.name, $scope.make.name, $scope.model.name, $scope.trim.name].join(' ');
        }

        return ymmt;
    };

    $scope.changed = function (type) {
        switch (type) {
            case 'year': yearChange(); break;
            case 'make': makeChange(); break;
            case 'model': modelChange(); break;
            case 'trim': trimChange(); break;
        }
    };

    function yearChange () {
        $scope.make = null;
        $scope.model = null;
        $scope.trim = null;

        $http.get('/api/decode/' + $scope.year.id).success(apiCallback);
    }

    function makeChange () {
        $scope.model = null;
        $scope.trim = null;
        $http.get('/api/decode/' + $scope.year.id + '/' + $scope.make.id).success(apiCallback);
    }

    function modelChange () {
        $scope.trim = null;
        $http.get('/api/decode/' + $scope.year.id + '/' + $scope.make.id + '/' + $scope.model.id).success(apiCallback);
    }

    function trimChange () {
        $rootScope.$emit('ymmtChange', {
            year: $scope.year,
            make: $scope.make,
            model: $scope.model,
            trim: $scope. trim
        });
    }

    function apiCallback (data) {
        var type = data.type,
            singularType = type.slice(0, -1),
            results = data.results,
            i, len;

        $scope[type] = results;

        if (typeof $scope[singularType] !== 'undefined' && $scope[singularType] !== null) {
            for (i=0, len=results.length; i<len; i++) {
                if (results[i].id === $scope[singularType]) {
                    $scope[singularType] = results[i];
                    break;
                }
            }
        }
    }

}]);

vvtControllers.controller('TransactionsCtrl', ['$scope', '$http', '$rootScope', function ($scope, $http, $rootScope) {

    $rootScope.$on('ymmtChange', function ($scope, $data) {
        if ($data) {
            $scope.$ymmt = $data;
        }
        console.log($scope.$ymmt);
    });

    $scope.ymmt = null;

    $scope.hasYMMT = function () {
        return $scope.ymmt() !== null;
    };

    $scope.ymmt = function () {
        var _ymmt = $scope.$ymmt;

        if (typeof _ymmt === 'undefined' || _ymmt === null || (
            'trim' in _ymmt && 'make' in _ymmt &&
            'model' in _ymmt && 'trim' in _ymmt
        )) {
            return null;
        }

console.log('$scope.ymmt');
console.log([_ymmt.year.name, _ymmt.make.name, _ymmt.model.name, _ymmt.model.trim].join(' '));
        return [_ymmt.year.name, _ymmt.make.name, _ymmt.model.name, _ymmt.model.trim].join(' ');
    };


}]);

vvtControllers.controller('BookValuesCtrl', ['$scope', '$http', function ($scope, $http) {}]);

vvtControllers.controller('CalculatorCtrl', ['$scope', '$http', function ($scope, $http) {}]);


