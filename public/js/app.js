var vvt = angular.module('vvtApp', ['MuiDirectives']);

// APP SERVICE //
vvt.provider('vvtService', function () {
    var obj = {
            year  : null,
            make  : null,
            model : null,
            trim  : null,
            ymmt : function () {
                var hasYMMT = true,
                    _ymmt = [obj.year, obj.make, obj.model, obj.trim];

                if (typeof obj.year !== 'string') {
                    hasYMMT = false;
                }
                if (typeof obj.make !== 'string') {
                    hasYMMT = false;
                }
                if (typeof obj.model !== 'string') {
                    hasYMMT = false;
                }
                if (typeof obj.trim !== 'string') {
                    hasYMMT = false;
                }

                console.log(hasYMMT);
                console.log(_ymmt.join(' '));

                return hasYMMT ? _ymmt.join(' ') : 'No YMMT :(';
            }
        };

    return {
        setYear: function (year) {
            obj.year = year;
        },
        setMake: function (make) {
            obj.make = make;
        },
        setModel: function (model) {
            obj.model = model;
        },
        setTrim: function (trim) {
            obj.trim = trim;
        },
        $get: function () {
            return obj;
        }
    };

});

// DECODER //
vvt.factory('DecoderApi', function ($http) {
    var apiUrl = '/api/decode';

    var DecoderApi = function () {};

    DecoderApi.prototype.fetch = function (config) {
        var path = [config.year, config.make, config.model, config.trim],
            url = apiUrl,
            endUrl = false,
            self = this;

        angular.forEach(path, function (val, key) {
            if (endUrl) {
                return;
            }

            if (typeof val !== 'undefined' && val !== null && val.id) {
                url += '/' + val.id;
            } else {
                endUrl = true;
            }
        });


        return $http.get(url).then(function (response) {
            self.results = response.data.results;
            self.type = response.data.type;

            return response;
        });
    };

    return DecoderApi;
});

vvt.factory('DecoderSrvc', function ($http, $rootScope, DecoderApi, vvtService) {
    var api = new DecoderApi(),
        obj = {
            year  : vvtService.year,
            make  : vvtService.make,
            model : vvtService.model,
            trim  : vvtService.trim,
            ymmt  : vvtService.ymmt,
            years : [],
            makes : [],
            models: [],
            trims : [],
            fetch : _fetch
        };

    function _fetch () {
        api.fetch(obj).then(function () {
            var type = api.type,
                singular = type.slice(0, -1),
                results = api.results;

            obj[type] = results;
        });
    }

    $rootScope.$on('ymmtChange', function () {
        console.log('after ymmtChange');
        console.log(arguments);
        console.log(vvtService.ymmt());
    })

    // initial load call
    _fetch();

    return obj;
});

vvt.controller('DecoderCtrl', function ($scope, $rootScope, DecoderSrvc) {
    console.log('DecoderCtrl');

    $scope.decoder = DecoderSrvc;

    $scope.changed = function (type) {
        if (type === 'trim') {
            $rootScope.$emit('ymmtChange');
        } else {
            $scope.decoder.fetch();
        }
    };
});

vvt.directive('decoder', function () {
    return {
        scope: '=',
        controller: 'DecoderCtrl'
    }
});



/// TRANSACTIONS
vvt.controller('TransactionCtrl', function ($scope, $rootScope, $http) {

    var _data = {
            headers: [
                { key: 'date',      label: 'Date Sold', sortable: true, formatter: 'date:mm/dd/yyyy' },
                { key: 'price',     label: 'Price',     sortable: true, formatter: 'currency' },
                { key: 'odometer',  label: 'Odo',       sortable: true, formatter: 'number' },
                { key: 'condition', label: 'Cond',      sortable: true },
                { key: 'engine',    label: 'Eng/Trans' },
                { key: 'color',     label: 'Color' },
                { key: 'type',      label: 'Type' },
                { key: 'region',    label: 'Region' },
                { key: 'auction',   label: 'Auction' },
            ],
            total: 0,
            transactions: [],
            sort: {}
        };

    $http.get('/api/transactions').then(function (response) {
        _data.total = response.data.total;
        _data.transactions = response.data.results;
    });

    $scope.sortBy = function (key, order) {
        if (typeof key === undefined) {
            return;
        }

        // allow for -key for reverse sorting
        if (key.charAt(0) === '-' && typeof order === 'undefined') {
            order = false;
            key = key.substr(1);
        }

        // get order based on new key value
        if (typeof order === 'undefined') {
            if (_data.sort.by === key) {
                // toggle
                order = !_data.sort.order;
            } else {
                // new key
                order = true;
            }
        }

        _data.sort = {
            by: key,
            order: !!order
        };
    }

    $scope.data = _data;
});

vvt.directive('transactions', function () {
    return {
        scope: '=',
        controller: 'TransactionCtrl'
    }
});
