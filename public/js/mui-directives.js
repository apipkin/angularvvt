var dirs = angular.module('MuiDirectives', []);


dirs.directive('resize', function ($window) {

    return {
        controller: function ($scope) {
            $scope.updateWindowSize = function () {
                $scope.winWidth = $window.innerWidth;
                $scope.winHeight = $window.innerHeight;
            };

            $scope.updateWindowSize();

            angular.element($window).bind('resize', function () {
                $scope.updateWindowSize();
                $scope.$apply();
            });
        },

        scope: {
            winHeight: '&',
            winWidth: '&'
        }
    };

});



dirs.controller('PanelCtrl', function ($scope) {
    $scope.title = '::panel::';
    $scope.visible = false;
    $scope.mode = 'inline';
    $scope.classname;

    $scope.togglePanel = function () {
        $scope.visible = !$scope.visible;
    };

    $scope.$watch('mode', function (oldVal, newVal, scope) {
        switch (newVal) {
            case 'overlay':
                $scope.classname = 'panel-overlay';
                break;
            case 'modal':
                $scope.classname = 'panel-modal';
                break;
            default:
                $scope.classname = 'panel-inline';
                break;
        }
    });
});

dirs.directive('muiPanel', function () {

    return {
        restrict: 'E',
        scope: {
            title: '&',
            visible: '&',
            mode: '&',
            classname: '@'
        },
        controller: 'PanelCtrl',
        transclude: true,
        template: '<div class="panel {{classname}}">' +
            '<div class="panel-trigger"><button ng-click="togglePanel()">{{title}}</button></div>' +
            '<div class="panel-content" ng-show="visible" ng-transclude></div>' +
            '</div>',
        link: function (scope, elem, attrs) {
            scope.title = attrs.panelTitle;
            scope.mode = attrs.panelMode;
        }
    }
});


